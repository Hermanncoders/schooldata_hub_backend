# schooldata_hub_backend

Tool for the collaborative documenting of:

- attendance lists
- tickable lists
- documenting parent / tutor authorizations
- workbook catalogues of the pupils
- learning progress in diverse / inclusive school settings with goal settings and checks
- competence charts with item statuses and image documentation
- extensive pupil profiles.
The information in the backend is with an id anonymized - no personal data is stored. The matching of the data with a name takes place locally in the client with a qr code.